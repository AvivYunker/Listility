import { IoBarChartSharp } from 'react-icons/io5'
import { MdQueryStats } from 'react-icons/md'
import { FaWpforms } from 'react-icons/fa'
import { ImProfile } from 'react-icons/im'

const links = [
  { id: 1, text: 'all lists', path: 'all-lists', icon: <MdQueryStats /> },
  { id: 2, text: 'Settings', path: 'settings', icon: <FaWpforms /> },
  { id: 3, text: 'About', path: 'about', icon: <ImProfile /> },
]

export default links
